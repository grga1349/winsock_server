# include <iostream>
# include "thread.h"

int main() 
{
    // port number variable
    int a;
    // thread class object
    Thread tr;
    
    // server logo 
    std::cout<<"     //////////////////////////////////////////////////////////////////// "<<std::endl;
    std::cout<<"    ///                                                              /// "<<std::endl;
    std::cout<<"   ///             WIN TCP/IP SERVER   v1.0                         /// "<<std::endl;
    std::cout<<"  ///                     by Ivan Grga 2016                        /// "<<std::endl;
    std::cout<<" ///                                                              /// "<<std::endl;
    std::cout<<"//////////////////////////////////////////////////////////////////// "<<std::endl;
    
    std::cout<<"Set the server port: ";
    // user input of port number
    std::cin>>a;
    // starting and running server at given port 
    tr.clConn(a); 
	system("pause"); 
	return 0;
}
