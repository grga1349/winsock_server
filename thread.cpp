# include "thread.h"

// class constr.
// setting id of connection to 0
Thread::Thread(void): n(0) 
{
}

// class destr.
Thread::~Thread()
{	
}

// method where client connects
// and new thread is started for each
// new connection
void Thread::clConn(int a)
{
	// declaration of socket class 
	// starts server on "a" port
	Socket sc(a);
	sc.set_c_id();// ->PRIVREMENO DOK SE NE RIJESI PROBLEM
	m.lock();
	std::cout<<"Server Log: Server sucessfully started on port: "<<a<<"."<<std::endl;
	m.unlock();
	// main server loop
	while (true) //<-POTREBNO PROMJENITI UVJET!!
	{
		// calling method that waits for connection
		sc.wtForConn();	
		// starting thread for new connection
		clThread.push_back(std::thread (&Socket::clFunct,&sc,(sc.tempSocket)));
		// incrementing connection id 
		n++;
		m.lock();
		// free unused memory
		int n2=0;
		sc.It2=sc.clStore.begin();
		while (sc.It2!=sc.clStore.end())
		{
			if(sc.clStore[n2]==SOCKET_ERROR)
			{
				sc.It2++;
				// free unused socket
			    sc.clStore.erase((sc.clStore.begin()+(n2)));
			    // join unused thread
			    clThread[(n2)].join();
			    // free unused thread
			    clThread.erase((clThread.begin()+(n2)));
			    std::cout<<"Server Log: Unused memory freed"<<std::endl;
				n2++;
			}
			else
			{
			    sc.It2++;
				n2++;	
			}
		}
		m.unlock();
	}
}


