#ifndef SOCKET_H
#define SOCKET_H

# include <iostream>
# include <winsock2.h>
# pragma comment(lib,"ws2_32.lib")
# include <vector>
# include <iterator>
# include <mutex>



class Socket
{
	public:
		//winsocket init.
	    WSADATA WsaDat;
	    //main socket
	    SOCKET mainSocket;
	    // temp. socket
	    SOCKET tempSocket;
	    //connected sockets storage 
	    std::vector<SOCKET> clStore;
	    //iterator
	    std::vector<SOCKET>::iterator It;
	    //iterator 2
	    std::vector<SOCKET>::iterator It2;
	    //socket setup struct
	    SOCKADDR_IN serverInf;
	    //const.
	    Socket(int sPort);
	    //destr.
	    ~Socket();
	    //method for waiting connection
	    void wtForConn();
	    //client function 
		//to be used in thread, 
		//that is started when client connects
	    void clFunct(const SOCKET tempSocket);
	    // locker
	    std::mutex m;
	    // connection id
	    int c_id;
	    // set c_id
	    void set_c_id(); // <- PRIVREMENO 
};

#endif
