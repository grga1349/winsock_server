#ifndef THREAD_H
#define THREAD_H

# include <thread>
# include <mutex>
# include <iostream>
# include "socket.h"
# include <vector>
# include <mutex>

class Thread
{
	public:
		//constr.
		Thread(void);
		//destr.
		~Thread();
		// vector of threads
		std::vector<std::thread> clThread;
		// here the client connects 
		void clConn(int a);
	    //id of connection  
	    int n;
	    // lock
	    std::mutex m;
};

#endif


