# include "socket.h"

// class constructor
// socket and winsock are initialised here 
Socket::Socket(int sPort) 
{
	// winsock setup
	if(WSAStartup(MAKEWORD(2,2),&WsaDat)!=0)
	std::cout<<"Error: Winsock startup failed!"<<std::endl;
	else
	std::cout<<"Server Log: Winsock startup."<<std::endl;
	
	// socket setup
	mainSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(mainSocket==INVALID_SOCKET)
	std::cout<<"Error: Socket creation failed!"<<std::endl;
	else
	std::cout<<"Server Log: Main socket created."<<std::endl;
	
	// server setup
	serverInf.sin_family=AF_INET;
    serverInf.sin_addr.s_addr=INADDR_ANY;
    serverInf.sin_port=htons(sPort);
    
    // bind server setup with socket
    if(bind(mainSocket,(SOCKADDR*)(&serverInf),sizeof(serverInf))==SOCKET_ERROR)
    std::cout<<"Error: Unable to bind main socket to server info!"<<std::endl;
    else
	std::cout<<"Server Log: Main socket binded to server info."<<std::endl;
    
    //listen
    listen(mainSocket,1);
    
}

//class destructor
Socket::~Socket()
{
	// shutdown socket
	shutdown(mainSocket,SD_SEND);

	// close socket 
	closesocket(mainSocket);

	// cleanup winsock
	WSACleanup();
}

//client waiting method
void Socket::wtForConn()
{
	//waiting for client to connect
	tempSocket=SOCKET_ERROR;
	while(tempSocket==SOCKET_ERROR)
	{
		tempSocket=accept(mainSocket,NULL,NULL);
	}
	m.lock();
	std::cout<<"Server Log: New client connected with c_id="<<c_id<<"."<<std::endl;
	std::cout<<"Server Log: Client socket id="<<tempSocket<<"."<<std::endl;
	m.unlock();
	// PROBA
	struct sockaddr_in ime = {0};
    int addrsize = sizeof(ime);

	// getting id adress
	getpeername(tempSocket, (struct sockaddr*)&ime, &addrsize);
	// cout-ing ip adress of client 
	m.lock();
	std::cout<<"Server Log: Client ip adress="<<inet_ntoa(ime.sin_addr)<<"."<<std::endl;
	m.unlock();
	//pushing connected socket into vector of connections
	clStore.push_back(tempSocket);
	//incrementing connection id
	c_id++;
	
}

// method made for runing inside thread
// that is started inside methor or class
// where this object is declared
void Socket::clFunct(const SOCKET tSocket)
{
	// thread starts
	m.lock();
	std::cout<<"Server Log: New thread started with socket id="<<tSocket<<"."<<std::endl;
	m.unlock();
	// setting buffer
	char buffer[1000];
	// seting the buffer for data
	memset(buffer,0,999);
	// DEBUG
	int re;
	// recive from curent -> send to all loop
	while (re=recv(tSocket,buffer,1000,0)!=0)//<-cheching if client is connected, and reciving message 
	{
		m.lock();
	    std::cout<<"Server Log: Socket "<<tSocket<<". recived:"<<std::endl;
	    std::cout<<buffer<<std::endl;
	    m.unlock();
	    // iterator that traverses vector of connections
	    // set to begining of vector
	    It=clStore.begin();
	    // index number of connected client 
	    int n=0;
	    // send receved data to all clients
	    while(It!=clStore.end())
	    {
	    	// send
	    	if (clStore[n]!=SOCKET_ERROR)
	    	{
	    	m.lock();
	        std::cout<<"Server Log: Sending data from socket "<<tSocket<<"to socket "<<clStore[n]<<"."<<std::endl;
	        send(clStore[n],buffer,1000,0);
	        m.unlock();
			}
	        // increment iterator and socket number 
	        It++;
	        n++;	        
		}
		// sleep for a given time
		Sleep(300);
		// seting the buffer for data
	    memset(buffer,0,999);		
	}
	// lock
	m.lock();
	std::cout<<"Server Log: Client on id="<<tSocket<<" disconnected."<<std::endl;
	// set disconnected socket to socket error 
	int n=0;
	It2=clStore.begin();
	while (It2!=clStore.end())
	{
	    if (clStore[n]==tSocket)
		clStore [n]=SOCKET_ERROR;
		n++;
		It2++;	
	}
	// unlock
	m.unlock();
}

// set c_id
void Socket::set_c_id() // <- PRIVREMENO 
{
	c_id=0;
}







